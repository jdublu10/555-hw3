package edu.upenn.cis.stormlite.spout;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class FileInputSpout extends FileSpout {
    static Logger log = LogManager.getLogger(FileInputSpout.class);

	public FileInputSpout() {}
	
	@Override
	public String getFilename() {
		try {
			return Files.walk(Paths.get(this.inputDir)).filter(f -> f.toString().endsWith("." + workerIndex)).findAny().get().toString();
		} catch (IOException e) {
			e.printStackTrace();
			return "input.in." + this.workerIndex;
		}
	}

}
