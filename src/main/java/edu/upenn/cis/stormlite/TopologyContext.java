/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.stormlite;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.atomic.AtomicInteger;

import edu.upenn.cis.stormlite.routers.StreamRouter;
import edu.upenn.cis.stormlite.tasks.ITask;
import edu.upenn.cis.stormlite.tuple.Tuple;

/**
 * Information about the execution of a topology, including
 * the stream routers
 * 
 * @author zives
 *
 */
public class TopologyContext {
	Topology topology;
	
	Queue<ITask> taskQueue;
	
	public static enum STATE {INIT, MAPPING, REDUCING, IDLE};
	
	STATE state = STATE.INIT;
	
	int mapOutputs = 0;
	
	int reduceOutputs = 0;
	
	Map<String, Integer> sendOutputs = new HashMap<>();
	
	private List<String> results;
	
	private AtomicInteger reads;
	private AtomicInteger writes;
	private String jobName;
	private String inputDir;
	
	/**
	 * Mappings from stream IDs to routers
	 */
	Map<String,StreamRouter> next = new HashMap<>();
	
	public TopologyContext(Topology topo, Queue<ITask> theTaskQueue, Config config) {
		topology = topo;
		taskQueue = theTaskQueue;
		results = new ArrayList<>();
		jobName = config.get("job");
		inputDir = config.get("inputDir");
		reads = new AtomicInteger(0);
		writes = new AtomicInteger(0);
	}
	
	public Topology getTopology() {
		return topology;
	}
	
	public void setTopology(Topology topo) {
		this.topology = topo;
	}
	
	public void addStreamTask(ITask next) {
		taskQueue.add(next);
	}

	public STATE getState() {
		return state;
	}

	public void setState(STATE state) {
		this.state = state;
	}

	public int getMapOutputs() {
		return mapOutputs;
	}

	public void incMapOutputs(String key) {
		this.mapOutputs++;
	}

	public int getReduceOutputs() {
		return reduceOutputs;
	}

	public void incReduceOutputs(String key) {
		this.reduceOutputs++;
	}
	
	public synchronized void incSendOutputs(String key) {
		 if (!sendOutputs.containsKey(key))
			 sendOutputs.put(key, Integer.valueOf(0));
		 
		 sendOutputs.put(key,  Integer.valueOf(sendOutputs.get(key) + 1));
	}
	
	public synchronized Map<String, Integer> getSendOutputs() {
		return sendOutputs;
	}

//	public static enum STATE {INIT, MAPPING, REDUCING, IDLE};
	public static STATE toState(String st) {
		switch(st) {
		case "INIT": return STATE.INIT;
		case "MAPPING": return STATE.MAPPING;
		case "REDUCING": return STATE.REDUCING;
		case "IDLE": return STATE.IDLE;
		}
		throw new RuntimeException("Unrecognized state: " + st);
	}
	
	public synchronized List<String> getResults() {
		synchronized(results) {
			return results;
		}
	}

	public synchronized void addResult(Tuple input) {
		synchronized(results) {
			results.add(String.format("(%s,%s)",input.getStringByField("key"),input.getStringByField("value")));
		}
	}
	
	public synchronized void incReadValues() {
		reads.getAndIncrement();
	}

	public synchronized void incWrittenValues() {
		writes.getAndIncrement();
	}
	
	public synchronized int getReadValues() {
		return reads.get();
	}
	
	public synchronized int getWrittenValues() {
		return writes.get();
	}
	
	public String getJobName() {
		return jobName;
	}

	public String getInputDir() {
		return inputDir;
	}
	
}
