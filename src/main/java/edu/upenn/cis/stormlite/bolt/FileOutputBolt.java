package edu.upenn.cis.stormlite.bolt;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;
import java.util.UUID;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import edu.upenn.cis.stormlite.OutputFieldsDeclarer;
import edu.upenn.cis.stormlite.TopologyContext;
import edu.upenn.cis.stormlite.routers.StreamRouter;
import edu.upenn.cis.stormlite.tuple.Fields;
import edu.upenn.cis.stormlite.tuple.Tuple;

public class FileOutputBolt implements IRichBolt {
	static Logger log = LogManager.getLogger(FileOutputBolt.class);
	
	private String executorId = UUID.randomUUID().toString();
	
	private FileWriter fileWriter;
	private static final Fields SCHEMA = new Fields("");
	
	private String fname;
	
	private TopologyContext context;

	@Override
	public String getExecutorId() {
		return executorId;
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(getSchema());
	}

	@Override
	public void cleanup() {
		try {
			fileWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public boolean execute(Tuple input) {
		String s = format(input);
		try {
			fileWriter.write(s + "\n");
			fileWriter.flush();
			
		} catch (Exception e) {
			log.warn(e.getLocalizedMessage());
			log.warn("Unable to write " + s + " to " + fname);
			e.printStackTrace();
		}
		log.info("Wrote " + s + " to " + fname);
		synchronized(context) {
			context.addResult(input);
			context.incWrittenValues();
		}
		return true;
	}

	private String format(Tuple input) {
		String k =input.getStringByField("key");
		String v = input.getStringByField("value");
		return String.format("(%s,%s)", k,v);
	}

	@Override
	public void prepare(Map<String, String> stormConf, TopologyContext context, OutputCollector collector) {
		this.context = context;
		fname = stormConf.get("storageDir") + "/" + stormConf.get("outputFile");
		log.info("Preparting output bolt to output to file: " + fname);
		if(fname == null) {
			throw new RuntimeException("Output directory not specified!");
		}
		File f = new File(fname);
	    try {
	    	f.getParentFile().mkdirs();
			f.createNewFile();
		} catch (IOException e1) {
			log.fatal("Could not locate or create file " + fname + ": " + e1.getLocalizedMessage());
			e1.printStackTrace();
		}
		try {
			fileWriter = new FileWriter(fname,false);
		} catch (Exception e) {
			log.fatal("Could not open filewriter: " + e.getLocalizedMessage());
			e.printStackTrace();
		}
	}

	@Override
	public void setRouter(StreamRouter router) {
	}

	@Override
	public Fields getSchema() {
		return SCHEMA;
	}

}
