package edu.upenn.cis455.mapreduce;

import edu.upenn.cis.stormlite.bolt.OutputCollector;
import edu.upenn.cis.stormlite.tuple.Values;

public class CollectorContext implements Context {
	private OutputCollector c;
	
	public CollectorContext(OutputCollector collector) {
		c = collector;
	}

	@Override
	public void write(String key, String value, String sourceExecutor) {
		c.emit(new Values<Object>(key,value), sourceExecutor);
	}

}
