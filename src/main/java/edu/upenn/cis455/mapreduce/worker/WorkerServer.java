package edu.upenn.cis455.mapreduce.worker;

import static spark.Spark.*;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;

import edu.upenn.cis.stormlite.Config;
import edu.upenn.cis.stormlite.DistributedCluster;
import edu.upenn.cis.stormlite.TopologyContext;
import edu.upenn.cis.stormlite.distributed.WorkerHelper;
import edu.upenn.cis.stormlite.distributed.WorkerJob;
import edu.upenn.cis.stormlite.routers.StreamRouter;
import edu.upenn.cis.stormlite.tuple.Tuple;
import edu.upenn.cis455.mapreduce.RunJobRoute;
import edu.upenn.cis455.mapreduce.storage.StorageFactory;
import spark.Spark;

/**
 * Simple listener for worker creation 
 * 
 * @author zives
 *
 */
public class WorkerServer {
    static Logger log = LogManager.getLogger(WorkerServer.class);

    static DistributedCluster cluster = new DistributedCluster();

    static List<TopologyContext> contexts = new ArrayList<>();

    static List<String> topologies = new ArrayList<>();
    
    private WorkerServer(int port , URL masterUrl, String storageDir) throws MalformedURLException {
    	if(masterUrl.getPort() == -1) {
    		throw new RuntimeException("Master URL " + masterUrl + " has no port specified.");
    	}
        log.info("Creating server listener at socket " + port);
        
        cluster.setMasterUrl(masterUrl);
        cluster.setWorkerPort(port);

        port(port);
        final ObjectMapper om = new ObjectMapper();
        om.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL);
        Spark.post("/definejob", (req, res) -> {
        	
            WorkerJob workerJob;
            try {
                workerJob = om.readValue(req.body(), WorkerJob.class);
                workerJob.getConfig().put("storageDir", storageDir);
                
                log.info("Job created with config: " + workerJob.getConfig().toString());

                try {
                    log.info("Processing job definition request" + workerJob.getConfig().get("job") +
                            " on machine " + workerJob.getConfig().get("workerIndex"));
                    contexts.add(cluster.submitTopology(workerJob.getConfig().get("job"), workerJob.getConfig(), 
                            workerJob.getTopology()));

                    // Add a new topology
                    synchronized (topologies) {
                        topologies.add(workerJob.getConfig().get("job"));
                    }
                } catch (ClassNotFoundException e) {
                	log.fatal("Could not find class!");
                    e.printStackTrace();
                    return "Job could not be launched";
                }
                return "Job launched";
            } catch (IOException e) {
                e.printStackTrace();

                // Internal server error
                res.status(500);
                return e.getMessage();
            } 

        });

        Spark.post("/runjob", new RunJobRoute(cluster));

        Spark.post("/pushdata/:stream", (req, res) -> {
            try {
                String stream = req.params(":stream");
                Tuple tuple = om.readValue(req.body(), Tuple.class);

                log.debug("Worker received: " + tuple + " for " + stream);

                // Find the destination stream and route to it
                StreamRouter router = cluster.getStreamRouter(stream);

                if (contexts.isEmpty())
                    log.error("No topology context -- were we initialized??");

                TopologyContext ourContext = contexts.get(contexts.size() - 1);

                // TODO: handle tuple vs end of stream for our *local nodes only*
                // Please look at StreamRouter and its methods (execute, executeEndOfStream, executeLocally, executeEndOfStreamLocally)
                // JC note: is this correct? I think we should just be executing these locally...
                //If the message was sent to *us*, it's probably us who should be handling it!!
                if (!tuple.isEndOfStream()) {
                    ourContext.incSendOutputs(router.getKey(tuple.getValues()));
                	router.executeLocally(tuple, ourContext, tuple.getSourceExecutor());
                } else {
                	router.executeEndOfStreamLocally(ourContext, tuple.getSourceExecutor());
                }


                return "OK";
            } catch (IOException e) {
                e.printStackTrace();

                res.status(500);
                return e.getMessage();
            }

        });
        
        Spark.get("/shutdown", (req,res) -> {
        	log.info("Worker shutdown triggered.");
        	shutdown();
        	log.info("Shutting down server...");
        	stop();
        	log.info("Shutdown complete.");
        	return "Worker shut down.";
        });

    }

    public static void createWorker(Map<String, String> config) {
        if (!config.containsKey("workerList"))
            throw new RuntimeException("Worker spout doesn't have list of worker IP addresses/ports");

        if (!config.containsKey("workerIndex"))
            throw new RuntimeException("Worker doesn't know its worker ID");
        if (!config.containsKey("workerStorage"))
            throw new RuntimeException("Worker doesn't know its worker storage directory");
        else {
            String[] addresses = WorkerHelper.getWorkers(config);
            int workerIndex = Integer.valueOf(config.get("workerIndex"));
            String myAddress = addresses[workerIndex];
            StorageFactory.setStorageDir(config.get("workerStorage"));

            log.debug("Initializing worker " + myAddress);

            URL url;
            try {
                url = new URL(myAddress);

                new WorkerServer(url.getPort(),new URL("http://127.0.0.1:45555"),"storage");
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }
    }

    public static void shutdown() {
        synchronized(topologies) {
            for (String topo: topologies)
                cluster.killTopology(topo);
        }

        cluster.shutdown();
    }

    /**
     * Simple launch for worker server.  Note that you may want to change / replace
     * most of this.
     * 
     * @param args
     * @throws MalformedURLException
     */
    public static void main(String args[]) throws MalformedURLException {
        org.apache.logging.log4j.core.config.Configurator.setLevel("edu.upenn.cis455", Level.DEBUG);
        org.apache.logging.log4j.core.config.Configurator.setLevel("edu.upenn.cis", Level.DEBUG);
        if (args.length < 3) {
            System.out.println("Usage: WorkerServer [port number] [master host/IP]:[master port] [storage directory]");
            System.exit(1);
        }
        
        int port = Integer.valueOf(args[0]);
        log.debug("Initializing worker on port" + port);
        String storageDir = args[2];
        StorageFactory.setStorageDir(storageDir);
        log.debug("Worker storage set to " + args[2]);
		URL masterUrl;
		try {
			log.debug("Worker has Master IP as " + args[1]);
			masterUrl = new URL("http://" + args[1]);
			new WorkerServer(port,masterUrl,storageDir);
		} catch (MalformedURLException e) {
			log.error("Could not parse worker URL: " + e.getLocalizedMessage());
			e.printStackTrace();
		}

    }
}
