package edu.upenn.cis455.mapreduce.worker;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.time.Instant;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

import edu.upenn.cis.stormlite.Config;
import edu.upenn.cis.stormlite.TopologyContext;

public class WorkerUpdateDaemon implements Runnable {

	private TopologyContext ctx;
	private Instant lastPing;
	private final long SLEEP_TIME = 1000;
	private String updateUrl ;
	private AtomicBoolean quit;
	private int workerPort;
	private String jobName;
	
	public WorkerUpdateDaemon(URL masterUrl, TopologyContext ctx, AtomicBoolean quit, int workerPort) {
		this.ctx = ctx;
		lastPing = Instant.MIN;
		this.updateUrl = masterUrl.toString() + "/workerstatus";
		this.quit = quit;
		this.workerPort = workerPort;
		this.jobName = ctx.getJobName();
	}
	
	private void ping() throws IOException {
		String queryParams = makeQueryParams();
		URL url = new URL(updateUrl + "?" + queryParams);
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setRequestMethod("GET");
		if(conn.getResponseCode() != HttpURLConnection.HTTP_OK) {
			throw new RuntimeException("Woker status request failed");
		}

	}

	//TODO: finish query params
	private String makeQueryParams() {
		synchronized(ctx){
			StringBuilder sb = new StringBuilder();
			sb.append("port=" + workerPort + "&");
			sb.append("status=" + ctx.getState() + "&");
			switch(ctx.getState()) {
			case IDLE:
				sb.append("keysRead=" + 0 + "&");
				sb.append("keysWritten=" + ctx.getWrittenValues() + "&");
				break;
			case INIT:
				sb.append("keysRead=0&");
				sb.append("keysWritten=0&");
				break;
			case MAPPING:
				sb.append("keysRead=" + ctx.getReadValues() + "&");
				sb.append("keysWritten=" + ctx.getWrittenValues() + "&");
				break;
			case REDUCING:
				sb.append("keysRead=" + ctx.getReadValues() + "&");
				sb.append("keysWritten=" + ctx.getWrittenValues() + "&");
				break;
			}
			sb.append("job=" + jobName + "&");
			List<String> results = ctx.getResults();
			if(results.size() >= 100) {
				results.subList(0, 99);
			}
			List<String> escapedResults = results.stream().map(tpl -> URLEncoder.encode(tpl)).collect(Collectors.toList());
			String resultsStr = String.join(",",escapedResults);
			sb.append("results=" + resultsStr);
			return sb.toString();
		}
	}

	@Override
	public void run() {
		while(!quit.get()) {
			try {
				Thread.sleep(SLEEP_TIME);
				ping();
			} catch (InterruptedException e) {
				e.printStackTrace();
				break;
			} catch (IOException e) {
				e.printStackTrace();
			}
			lastPing = Instant.now();
		}
	}
	


}
