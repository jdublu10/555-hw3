package edu.upenn.cis455.mapreduce.worker;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import edu.upenn.cis.stormlite.TopologyContext;
import spark.QueryParamsMap;

public class WorkerStatus {
	private int workerId;
	private int port;
	private TopologyContext.STATE status;
	private String job;
	private int keysRead;
	private int keysWritten;
	private String results;
	
	private WorkerStatus(int id, int port, TopologyContext.STATE status, String job, int kr, int kw, String results) {
		this.workerId = id;
		this.port = port;
		this.status = status;
		this.job = job;
		this.keysRead = kr;
		this.keysWritten = kw;
		this.results = results;
	}

	public int getWorkerId() {
		return workerId;
	}

	public static WorkerStatus blank() {
		return new WorkerStatus(0,0,TopologyContext.STATE.INIT,"",0,0,"");
	}

	public static WorkerStatus build(Map<String, String[]> map, String ip, Map<String, Integer> workerIpToIndex) {
		int port = Integer.valueOf(map.get("port")[0]);
		TopologyContext.STATE status = TopologyContext.toState(map.get("status")[0]);
		String job = map.get("job")[0];
		int keysRead = Integer.valueOf(map.get("keysRead")[0]);
		int keysWritten = Integer.valueOf(map.get("keysWritten")[0]);
		String results = map.get("results")[0];
		int workerId = workerIpToIndex.get("http://" + ip + ":" + port);
		return new WorkerStatus(workerId,port,status,job,keysRead,keysWritten,results);
	}
	
	@Override
	public String toString() {
		String resultsStr = String.join(",",results);
		return String.format("%d: port=%d, status=%s, job=%s, keysRead=%d, keysWritten=%d, results=[%s]",
				workerId,port,status.toString(),job,keysRead,keysWritten,resultsStr);
	}
}
