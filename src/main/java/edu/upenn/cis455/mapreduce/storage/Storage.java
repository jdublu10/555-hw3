package edu.upenn.cis455.mapreduce.storage;

import java.io.File;
import java.util.HashSet;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.sleepycat.bind.serial.StoredClassCatalog;
import com.sleepycat.je.Database;
import com.sleepycat.je.DatabaseConfig;
import com.sleepycat.je.Environment;
import com.sleepycat.je.EnvironmentConfig;
import com.sleepycat.persist.EntityStore;
import com.sleepycat.persist.PrimaryIndex;
import com.sleepycat.persist.SecondaryIndex;
import com.sleepycat.persist.StoreConfig;
import com.sleepycat.persist.model.AnnotationModel;
import com.sleepycat.persist.model.EntityModel;

import edu.upenn.cis455.mapreduce.storage.models.ExecutorKeyAssignment;
import edu.upenn.cis455.mapreduce.storage.models.KeyValues;


//TODO: should this be a singleton?
public class Storage {
  final static Logger logger = LogManager.getLogger(Storage.class);
	private Environment env;

//	private static final String CLASS_CATALOG_NAME = "java_class_catalog";
//	private StoredClassCatalog javaCatalog;
	
//	private static final String USER_STORE_NAME = "UserStore";
//	private EntityStore userStore;
	
	private static final String MAPPED_KEY_VALUE_STORE_NAME = "MappedKeyValueStore";
	private static final String EXECUTOR_KEY_ASSIGNMENT_STORE_NAME = "ExecutorKeyAssignmentStore";
	private EntityStore mkvStore;
	private EntityStore ekaStore;
	
    private PrimaryIndex<Long,KeyValues> mkvById;
	private SecondaryIndex<String,Long,KeyValues> mkvByKey;
    private PrimaryIndex<String, ExecutorKeyAssignment> ekaByExecutorId;
	

    Storage(String dir) {
		logger.debug("Launching env in dir: " + dir);

		EnvironmentConfig envConfig = new EnvironmentConfig();
		//TODO: Add more config things here!
		envConfig.setTransactional(true);
		envConfig.setAllowCreate(true);
		env = new Environment(new File(dir), envConfig);
		logger.debug("Env created");
		
        EntityModel em = new AnnotationModel();
//        em.registerClass(InstantProxy.class);
        StoreConfig mkvsConfig = new StoreConfig().setAllowCreate(true).setTemporary(true).setModel(em);
        StoreConfig ekaConfig = new StoreConfig().setAllowCreate(true).setTemporary(true).setModel(em);

        mkvStore = new EntityStore(env, MAPPED_KEY_VALUE_STORE_NAME, mkvsConfig);
        ekaStore = new EntityStore(env,EXECUTOR_KEY_ASSIGNMENT_STORE_NAME,ekaConfig);
        
        mkvById = mkvStore.getPrimaryIndex(Long.class, KeyValues.class);
        mkvByKey = mkvStore.getSecondaryIndex(mkvById, String.class, "key");
        
        ekaByExecutorId = ekaStore.getPrimaryIndex(String.class, ExecutorKeyAssignment.class);

	}
    
    
    public void cleanup() {
    	logger.debug("Cleaning up Storage");
    	mkvStore.close();
    	ekaStore.close();
		env.close();
    }


	public synchronized void store(String key, String value, String executorId) {
		if(!ekaByExecutorId.contains(executorId)) {
			ekaByExecutorId.put(new ExecutorKeyAssignment(executorId));
		}
		
		ExecutorKeyAssignment eka = ekaByExecutorId.get(executorId);
		eka.addKeyIfAbsent(key);
		
		if(!mkvByKey.contains(key)) {
			mkvById.put(new KeyValues(key));
		}
		KeyValues kvs = mkvByKey.get(key);
		kvs.addValue(value);
		
		mkvById.put(kvs);
		ekaByExecutorId.put(eka);
	}
	
	public Set<KeyValues> kvsForExecutor(String executorId){
		ExecutorKeyAssignment eka = ekaByExecutorId.get(executorId);
		if(eka == null) {
			return new HashSet<>();
		}
		Set<String> keys = eka.getAssignedKeys();
		Set<KeyValues> kvs = new HashSet<>();
		for(String key : keys) {
			kvs.add(mkvByKey.get(key));
		}
		return kvs;
	}
}
