package edu.upenn.cis455.mapreduce.storage.models;

import java.util.HashSet;
import java.util.Set;

import com.sleepycat.persist.model.Entity;
import com.sleepycat.persist.model.PrimaryKey;
import com.sleepycat.persist.model.Relationship;
import com.sleepycat.persist.model.SecondaryKey;

@Entity
public class ExecutorKeyAssignment {
	
	@PrimaryKey
	private String executorId;
	
	@SecondaryKey(relate = Relationship.ONE_TO_MANY)
	private Set<String> keysOwned;
	
	public ExecutorKeyAssignment(String eId) {
		executorId = eId;
		keysOwned = new HashSet<>();
	}
	
	private ExecutorKeyAssignment() {}

	public void addKeyIfAbsent(String key) {
		keysOwned.add(key);
	}

	public Set<String> getAssignedKeys() {
		return keysOwned;
	}

}
