package edu.upenn.cis455.mapreduce.storage;

public class StorageFactory {
	
	private static Storage INSTANCE;
	private static boolean set;

	public static Storage getInstance() {
		if(INSTANCE == null)
			throw new RuntimeException("Attempted to get storage before directory set.");
		return INSTANCE;
	}
	
	public static void setStorageDir(String dir) {
		if(set)
			throw new RuntimeException("Storage directory already set.");
		INSTANCE = new Storage(dir);
		set = true;
	}

}
