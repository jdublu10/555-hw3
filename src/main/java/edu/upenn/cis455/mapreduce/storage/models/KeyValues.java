package edu.upenn.cis455.mapreduce.storage.models;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.sleepycat.persist.model.Entity;
import com.sleepycat.persist.model.PrimaryKey;
import com.sleepycat.persist.model.Relationship;
import com.sleepycat.persist.model.SecondaryKey;

@Entity
public class KeyValues {
	
	@PrimaryKey(sequence="ID")
	private long id;
	
	@SecondaryKey(relate=Relationship.ONE_TO_ONE)
	private String key;

	private List<String> values;

	public KeyValues(String key) {
		this.key = key;
		this.values = new ArrayList<>();
	}

	private KeyValues() {}

	public void addValue(String value) {
		values.add(value);
	}

	public String getKey() {
		return key;
	}
	
	public List<String> getValues(){
		return values;
	}

}
