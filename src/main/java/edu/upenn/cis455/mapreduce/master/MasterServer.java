package edu.upenn.cis455.mapreduce.master;

import static spark.Spark.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import edu.upenn.cis.stormlite.Config;
import edu.upenn.cis.stormlite.Topology;
import edu.upenn.cis.stormlite.TopologyBuilder;
import edu.upenn.cis.stormlite.bolt.FileOutputBolt;
import edu.upenn.cis.stormlite.bolt.IRichBolt;
import edu.upenn.cis.stormlite.bolt.MapBolt;
import edu.upenn.cis.stormlite.bolt.ReduceBolt;
import edu.upenn.cis.stormlite.distributed.WorkerHelper;
import edu.upenn.cis.stormlite.distributed.WorkerJob;
import edu.upenn.cis.stormlite.spout.FileSpout;
import edu.upenn.cis.stormlite.spout.FileInputSpout;
import edu.upenn.cis.stormlite.tuple.Fields;
import edu.upenn.cis455.mapreduce.worker.WorkerStatus;


public class MasterServer {  
	private static final String WORD_SPOUT = "WORD_SPOUT";
    private static final String MAP_BOLT = "MAP_BOLT";
    private static final String REDUCE_BOLT = "REDUCE_BOLT";
    private static final String WRITE_BOLT = "WRITE_BOLT";
	
	static Logger log = LogManager.getLogger(MasterServer.class);
	
	private static Map<String,Integer> workerIpToIndex = new HashMap<>();
	private static ArrayList<WorkerStatus> statuses = new ArrayList<>();
	private static String[] workers = {"http://127.0.0.1:8001","http://127.0.0.1:8002"};
	
	private static void registerShutdownAction() {
		get("/shutdown", (req,res) -> {
			log.info("Shutdown initiated, sending shutdown messages to workers.");
			shutdownWorkers();
			log.info("Worker shtudown complete, stopping server.");
			stop();
			return "Shut down.";
		});
	}

	private static void shutdownWorkers() {
		log.info("Shutting down workers...");
		if(workers == null) {
			return;
		}
		for(String worker : workers) {
			try {
				URL url = new URL(worker + "/shutdown");
				log.info("Sending shutdown message: " + url.toString());
				HttpURLConnection conn = (HttpURLConnection) url.openConnection();
				conn.getResponseCode();
			} catch (MalformedURLException e) {
				log.warn(e.getLocalizedMessage());
				e.printStackTrace();
			} catch (IOException e) {
				log.warn(e.getLocalizedMessage());
				e.printStackTrace();
			}
		}
	}
	
	private static WorkerJob makeMRJob(Config config) {
		FileSpout spout = new FileInputSpout();

		MapBolt map = new MapBolt();
		ReduceBolt reduce = new ReduceBolt();
		FileOutputBolt write = new FileOutputBolt();
		TopologyBuilder builder = new TopologyBuilder();

		builder.setSpout(WORD_SPOUT, spout, Integer.valueOf(config.get("spoutExecutors")));
		builder.setBolt(MAP_BOLT, map, Integer.valueOf(config.get("mapExecutors"))).fieldsGrouping(WORD_SPOUT, new Fields("value"));
		builder.setBolt(REDUCE_BOLT, reduce, Integer.valueOf(config.get("reduceExecutors"))).fieldsGrouping(MAP_BOLT, new Fields("key"));
	    builder.setBolt(WRITE_BOLT, write, 1).firstGrouping(REDUCE_BOLT);
		Topology topo = builder.createTopology();
		return new WorkerJob(topo, config);
	}
	
	private static void dispatchJob(WorkerJob job, Config config) throws IOException {
		ObjectMapper mapper = new ObjectMapper();
		mapper.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL);
		try {
			workers = WorkerHelper.getWorkers(config);
			System.out.println("Job built. sending to workers: " + workers);

			int i = 0;
			for (String dest: workers) {
				workerIpToIndex.put(dest, i);
				statuses.add(WorkerStatus.blank());
				
				config.put("workerIndex", String.valueOf(i++));
				System.out.println("Defining job for " + dest);
				if (sendJob(dest, "POST", config, "definejob", 
						mapper.writerWithDefaultPrettyPrinter().writeValueAsString(job)).getResponseCode() != 
						HttpURLConnection.HTTP_OK) {
					throw new RuntimeException("Job definition request failed");
				}
			}
			for (String dest: workers) {
				System.out.println("Running job on " + dest);
				if (sendJob(dest, "POST", config, "runjob", "").getResponseCode() != 
						HttpURLConnection.HTTP_OK) {
					throw new RuntimeException("Job execution request failed");
				}
			}
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(0);
		}
	}
	
	private static void registerWorkerStatusRoute() {
		get("/workerstatus",(req,res) -> {
			WorkerStatus status = WorkerStatus.build(req.queryMap().toMap(),req.ip(),workerIpToIndex);
			statuses.set(status.getWorkerId(), status);
			return "";
		});
		
	}
	
    public static void registerStatusPage() {
        get("/status", (request, response) -> {
            response.type("text/html");
            String formText = "<form method=\"POST\" action=\"/submitjob\">\r\n"
            + "    Job Name: <input type=\"text\" name=\"jobname\"/><br/>\r\n"
            + "    Class Name: <input type=\"text\" name=\"classname\"/><br/>\r\n"
            + "    Input Directory: <input type=\"text\" name=\"input\"/><br/>\r\n"
            + "    Output Directory: <input type=\"text\" name=\"output\"/><br/>\r\n"
            + "    Map Threads: <input type=\"text\" name=\"map\"/><br/>\r\n"
            + "    Reduce Threads: <input type=\"text\" name=\"reduce\"/><br/>\r\n"
            + "<input type=\"submit\" value=\"Submit\">"
            + "</form>";

            System.out.println("Serving /status");

            String identification = "<p>Created by Joseph Cutler (jwc@seas.upenn.edu)</p>";
            
            StringBuilder sb = new StringBuilder();
            sb.append("<html><head><title>Master</title></head>\n<body>");
            sb.append(identification);
            
            sb.append("<ul>");
            for(WorkerStatus status : statuses) {
            	sb.append("<li>");
            	sb.append(status.toString());
            	sb.append("</li>");
            }
            sb.append("</ul>");
            
            sb.append(formText);
            sb.append("</body></html>");
            
            return sb.toString();

        });
    }
    
    private static void registerSubmitJobAction() {
    	post("/submitjob",(req,res) -> {
    		Map<String,String[]> query = req.queryMap().toMap();
    		
    		String jobName = query.get("jobname")[0];
    		String className = query.get("classname")[0];
    		String outputDir = query.get("output")[0];
    		String inputDir = query.get("input")[0];
    		String mapThreads = query.get("map")[0];
    		String reduceThreads = query.get("reduce")[0];

    		Config config = new Config();
    		config.put("workerList", "[127.0.0.1:8001,127.0.0.1:8002]");
    		config.put("outputFile", outputDir + "/output.txt");
    		config.put("outputDir", outputDir);
    		config.put("inputDir", inputDir);
    		config.put("job", jobName);
    		config.put("mapClass", className);
    		config.put("reduceClass", className);
    		config.put("spoutExecutors", "1");
    		config.put("mapExecutors", mapThreads);
    		config.put("reduceExecutors", reduceThreads);
    		
    		log.info("Creating job with parameters: " + config);
    		
    		WorkerJob job = makeMRJob(config);
    		dispatchJob(job,config);
    		
    		return "Submitted job to workers.";
    	});
    }
    
    static HttpURLConnection sendJob(String dest, String reqType, Config config, String job, String parameters) throws IOException {
		URL url = new URL(dest + "/" + job);
		
		log.info("Sending request to " + url.toString());
		
		HttpURLConnection conn = (HttpURLConnection)url.openConnection();
		conn.setDoOutput(true);
		conn.setRequestMethod(reqType);
		
		if (reqType.equals("POST")) {
			conn.setRequestProperty("Content-Type", "application/json");
			
			OutputStream os = conn.getOutputStream();
			byte[] toSend = parameters.getBytes();
			os.write(toSend);
			os.flush();
		} else
			conn.getOutputStream();
		
		return conn;
    }
    
    /**
     * The mainline for launching a MapReduce Master.  This should
     * handle at least the status and workerstatus routes, and optionally
     * initialize a worker as well.
     * 
     * @param args
     * @throws IOException 
     */
    public static void main(String[] args) throws IOException {
        org.apache.logging.log4j.core.config.Configurator.setLevel("edu.upenn.cis455", Level.DEBUG);
        org.apache.logging.log4j.core.config.Configurator.setLevel("edu.upenn.cis", Level.DEBUG);
        if (args.length < 1) {
            System.out.println("Usage: MasterServer [port number]");
            System.exit(1);
        }

        int myPort = Integer.valueOf(args[0]);
        port(myPort);

        log.info("Master node startup, on port " + myPort);

        registerStatusPage();
        registerWorkerStatusRoute();
        registerSubmitJobAction();
        registerShutdownAction();

    }

	

	
}

